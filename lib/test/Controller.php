<?php

namespace test;

class Controller
{
	protected $tpl;
	protected $req;

	public function __construct()
	{
		$this->tpl = S('Template');
		$this->req = S('Request');
		$action = $this->req->getDir(0);

		if ( in_array($action, array('subscribe', 'unsubscribe', null)) )
		{
			$this->tpl->setGlob('baseurl', '');
		}
		else
		{
			$this->tpl->setGlob('baseurl', "/$action");
		}

		session_start();

		$this->_performChecks();
	}

	protected function _subscribe()
	{
		$cfg = S('Config')->getName();
		$_SESSION[$cfg]['subs'] = 1;
	}

	protected function _unsubscribe()
	{
		$cfg = S('Config')->getName();
		$_SESSION[$cfg]['subs'] = 0;
	}

	private function _isSubscribed()
	{
		$cfg = S('Config')->getName();
		return isset($_SESSION[$cfg]['subs']) ? $_SESSION[$cfg]['subs'] : 0;
	}

	private function _performChecks()
	{
		$action = S('Request')->getDir(0);
		if( !in_array($action, array('subscribe', 'unsubscribe', null)) )
		{
			$action = S('Request')->getDir(1);
		}

		if( !$this->_isSubscribed() && ($action != 'subscribe') )
		{
			Response::redirect('/subscribe');
		}
	}
}
